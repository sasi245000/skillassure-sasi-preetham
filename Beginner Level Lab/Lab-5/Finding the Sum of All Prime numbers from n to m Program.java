/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	   Scanner sc=new Scanner(System.in);
	   
	   //Goal - Finding the Sum of All Prime numbers from n to m
        int i,num,count, sum=0;
        
        //Taking the Input values from the User
        System.out.print("Please Enter n value: ");//Enter the value of n
        int n=sc.nextInt();
        System.out.print("Please Enter m value: ");//Enter the value of n
        int m=sc.nextInt();
       
        //loop for finding and printing all prime numbers in given range
        for(num=n; num<=m; num++){
        count=0;
        for(i=2; i<=num/2; i++){
            if(num%i==0){
            count++;
            break;
            
        }
    }
        if(count==0 && num !=1){
        sum+=num;
        
        //Displaying All Prime Number from n to m
        System.out.println(+num+" ");
        }
        }
        
        //Displaying the sum of All Prime Numbers from n to m
        System.out.println("Sum of Prime Numbers from "+n+" to "+m+" : "+sum+"");
        }
        }
	

