/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	    //Goal - Swapping of Numbers
	    int number3;
		Scanner sc= new Scanner(System.in);
		
	    //Step 1 - Taking the Input from the User
	    
        System.out.print("Enter a Number - "); //Enter Number 1
        int number1= sc.nextInt(); 
        
        System.out.print("Enter a Number - "); //Enter Number 2
        int number2= sc.nextInt(); 
        
        
        //Displaying the values Before Swapping
        System.out.println("Before Swapping of Numbers : "+number1 +"  "+ number2);
        
        //Swapping of Numbers
        number3 = number1;  
        number1 = number2;  
        number2 = number3;  
        
        //Displaying the Value After Swapping
       System.out.println("After Swapping : "+number1 +"   " + number2);  
       
	}
}
