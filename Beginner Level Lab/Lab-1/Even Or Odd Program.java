/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	   //Goal - Checking If Number is Even Or Odd
	    Scanner sc= new Scanner(System.in);
		
	//Step 1 - Taking the Input from the User
	    
        System.out.print("Enter a Number - "); //Enter a Number
        int number= sc.nextInt();  
        
        //Checking if Number is Even Or Odd
        if(number % 2 == 0)
        {
            //Displaying the Result if Number is Even
            System.out.println("Number Is Even");
        }
        else{
            //Displaying the Result if Number is Odd
            System.out.println("Number Is Odd");
        }
	}
}
