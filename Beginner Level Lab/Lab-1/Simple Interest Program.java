/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	    //Goal - Calculate Simple Interest
		Scanner sc= new Scanner(System.in);
		
	    //Step 1 - Accepting the Variable from the User
        System.out.print("Enter Principle - "); //Enter Value for Principle
        double Principle= sc.nextDouble();  
        
        System.out.print("Enter Time - ");  //Enter Value for Time
        double Time= sc.nextDouble();  
        
        System.out.print("Enter Rate - ");  //Enter Value for Rate
        double Rate= sc.nextDouble();  
        
        //Implementing the formula to Calculate Simple Interest
        double SimpleInterest= (Principle * Time * Rate)/100;  
        
        //Displaying the value of Simple Interest
        System.out.println("Simple Interest = " +SimpleInterest);  
	}
}
