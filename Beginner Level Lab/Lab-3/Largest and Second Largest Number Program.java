/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	    //Goal - Finding the Largest Number and Second Largest Number
		Scanner sc= new Scanner(System.in);
		
	    //Step 1 - Taking the Input from the User
	    
        System.out.print("Enter a Number 1 - "); //Enter Number 1
        int a= sc.nextInt(); 
        
        System.out.print("Enter a Number 2 - "); //Enter Number 2
        int b= sc.nextInt(); 
        
        System.out.print("Enter a Number 3 - "); //Enter Number 2
        int c= sc.nextInt(); 
        
        //Checking for Largest Number among the values
        if(a > b && a > c)
        {
            //Dsiplaying the Result
            System.out.println("Largest Number is : "+a);
        }
        else if(b > c )
        {
            //Dsiplaying the Result
            System.out.println("Largest Number is : "+b);
        }
        else
        {
            //Dsiplaying the Result
            System.out.println("Largest Number is : "+c);
        }
        
        //Checking for Second Largest Number
        if (a > b && a < c || a > c && a < b)
            //Dsiplaying the Result
            System.out.println("Second Largest Number is "+a);
            
        else if (b > a && b < c || b > c && b < a)
            //Dsiplaying the Result
            System.out.println("Second Largest Number is "+b);
            
        else
            //Dsiplaying the Result
            System.out.println("Second Largest Number is "+c);
       
	}
}
