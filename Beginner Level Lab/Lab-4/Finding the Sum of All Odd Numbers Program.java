/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	    //Goal - Find the Sum of All Odd Numbers from 1 to N
	    int N, i, sum = 0;
 
        Scanner sc= new Scanner(System.in);
       	
       	//Taking the value of N from the User
        System.out.print("Enter a value of N - ");
        N = sc.nextInt();
 
        //Finding and Iterating over all Numbers from 1 to N
        for(i = 1; i <= N; i++){
            if((i%2) == 1){
                sum += i;
            }
        }
     
        //Displaying the Result
        System.out.print("Sum of all Odd Numbers from 1 to "
            + N + " = " + sum);
 
	}
}
