/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	    //Goal - Finding the Series or Pattern
	    Scanner sc=new Scanner(System.in);
	    
	    //Taking Input Value from the User
	    System.out.print("Enter the Number - ");
	    int num = sc.nextInt();
	    int sum = 1;
	    int sign = 1;
	    
	    //For Loop for Iterating over each value
	    for(int i = 1;i<=num;i++)
	    {
	        //Displaying the Result
	        System.out.print(sum*sign+",");
	        sum = sum+(i*i);
	        sign = sign*-1;
	    }
	   
       
        }
        }
	

