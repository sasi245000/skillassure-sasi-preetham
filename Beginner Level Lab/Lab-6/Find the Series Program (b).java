/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	    //Goal - Finding the Series or Pattern
	    Scanner sc=new Scanner(System.in);
	    
	    //Taking Input Value from the User
	    System.out.print("Enter the Number - ");
	    int num = sc.nextInt();
	    
	    
	    //For Loop for Iterating over each value
	    for (int i = 2; i <=2*num; i+=2) 
        {
            int result =(i*i); 
            //Displaying the Result
            System.out.print(result+" ");
        } 
	   
       
        }
        }
	

