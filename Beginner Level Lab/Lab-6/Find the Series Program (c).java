/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.util.*;  
public class Main
{
	public static void main(String[] args) {
	   //Goal - Finding the Series or Pattern
	   int n1=1,n2=1,n3,i;
	   Scanner sc=new Scanner(System.in);
	    
	   //Taking Input Value from the User
	   System.out.print("Enter the Number - ");
	   int num = sc.nextInt();
	   
	   //Displaying the values of 1 and 2
       System.out.print(n1+" "+n2);//printing 0 and 1    
    
       //Loop starts from 2 
       for(i=2;i<num;++i)   
        {    
            n3=n1+n2;    
            System.out.print(" "+n3);    
            n1=n2;    
            n2=n3;    
            }    
  
            }
    
}  
	

